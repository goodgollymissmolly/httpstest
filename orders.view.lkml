view: orders {
  sql_table_name: demo_db.orders ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension_group: created {
    type: time
    timeframes: [
      raw,
      time,
      date,
      week,
      month,
      quarter,
      year
    ]
    sql: ${TABLE}.created_at ;;
  }

  dimension: millions {
    type:  number
    sql:  case when ${id}<100 then 130000
          when ${id}=101 then 400000
          when ${id}>100 then 30000 end;;
    value_format: "0.000,,\" M\""
  }

  dimension: status {
    type: string
    sql: ${TABLE}.status ;;
    #suggest_persist_for: "1 minutes"
    bypass_suggest_restrictions: yes
  }

  dimension: blah {
    type: string
    sql: 'blah' ;;
  }

  dimension: user_id {
    type: number
    # hidden: yes
    sql: ${TABLE}.user_id ;;
  }

  dimension: break {
    type: string
    html: <div style="white-space:pre">{{ value }}</div> ;;
    sql: concat(${blah},  "\r\n" , ${status}) ;;
  }


  dimension: concat {
    type: string
    # hidden: yes
    sql: concat(${blah},  "\r\n" , ${status}) ;;
  }

  measure: count {
    type: count
    drill_fields: [id, users.first_name, users.last_name, users.id, order_items.count]
  }
}
