view: teachers {
  sql_table_name: nested_practice_data.teachers ;;

  dimension: id {
    primary_key: yes
    type: number
    sql: ${TABLE}.id ;;
  }

  dimension: department {
    type: string
    sql: ${TABLE}.department ;;
  }

  dimension: first_name {
    type: string
    sql: ${TABLE}.first_name ;;
  }

  dimension: last_name {
    type: string
    sql: ${TABLE}.last_name ;;
  }

  dimension: room_num {
    type: string
    sql: ${TABLE}.room_num ;;
  }

  dimension: salary {
    type: number
    sql: ${TABLE}.salary ;;
  }

  dimension: schedule {
    hidden: yes
    sql: ${TABLE}.schedule ;;
  }



  measure: count {
    type: count
    drill_fields: [id, last_name, first_name]
  }
}

view: teachers__schedule {
  dimension: class_name {
    type: string
    sql: ${TABLE}.class_name ;;
  }

  dimension: period {
    type: number
    sql: ${TABLE}.period ;;
  }
}
