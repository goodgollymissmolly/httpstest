- dashboard: calvin_klein
  title: Calvin Klein
  layout: newspaper
  elements:
  - title: Calvin Klein Products by Gender
    name: Calvin Klein Products by Gender
    model: bitbucketz
    explore: inventory_items
    type: looker_bar
    fields:
    - products.department
    - products.count
    - products.category
    pivots:
    - products.category
    filters:
      products.brand: "%Calvin Klein%"
    sorts:
    - products.count desc 0
    - products.category
    limit: 500
    column_limit: 50
    stacking: normal
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#24ed2b"
    show_null_points: true
    point_style: circle
    colors:
    - 'palette: Santa Cruz'
    series_colors: {}
    series_types: {}
    y_axis_reversed: false
    y_axes:
    - label: ''
      maxValue:
      minValue:
      orientation: top
      showLabels: true
      showValues: true
      tickDensity: default
      tickDensityCustom: 5
      type: linear
      unpinAxis: false
      valueFormat:
      series:
      - id: Accessories
        name: Accessories
        axisId: products.count
      - id: Active
        name: Active
        axisId: products.count
      - id: Blazers & Jackets
        name: Blazers &amp; Jackets
        axisId: products.count
      - id: Clothing Sets
        name: Clothing Sets
        axisId: products.count
      - id: Dresses
        name: Dresses
        axisId: products.count
      - id: Fashion Hoodies & Sweatshirts
        name: Fashion Hoodies &amp; Sweatshirts
        axisId: products.count
      - id: Intimates
        name: Intimates
        axisId: products.count
      - id: Jeans
        name: Jeans
        axisId: products.count
      - id: Leggings
        name: Leggings
        axisId: products.count
      - id: Outerwear & Coats
        name: Outerwear &amp; Coats
        axisId: products.count
      - id: Pants
        name: Pants
        axisId: products.count
      - id: Pants & Capris
        name: Pants &amp; Capris
        axisId: products.count
      - id: Plus
        name: Plus
        axisId: products.count
      - id: Shorts
        name: Shorts
        axisId: products.count
      - id: Skirts
        name: Skirts
        axisId: products.count
      - id: Sleep & Lounge
        name: Sleep &amp; Lounge
        axisId: products.count
      - id: Socks
        name: Socks
        axisId: products.count
      - id: Socks & Hosiery
        name: Socks &amp; Hosiery
        axisId: products.count
      - id: Suits & Sport Coats
        name: Suits &amp; Sport Coats
        axisId: products.count
      - id: Sweaters
        name: Sweaters
        axisId: products.count
      - id: Swim
        name: Swim
        axisId: products.count
      - id: Tops & Tees
        name: Tops &amp; Tees
        axisId: products.count
      - id: Underwear
        name: Underwear
        axisId: products.count
    hidden_fields: []
    listen:
      Date: inventory_items.created_date
      Category: products.category
    row: 8
    col: 8
    width: 16
    height: 7
  - title: Calvin Klein Price of Products
    name: Calvin Klein Price of Products
    model: bitbucketz
    explore: inventory_items
    type: looker_scatter
    fields:
    - products.brand
    - inventory_items.expensive
    - products.count
    pivots:
    - inventory_items.expensive
    fill_fields:
    - inventory_items.expensive
    filters:
      products.brand: Calvin Klein
    sorts:
    - products.brand
    - inventory_items.expensive desc
    limit: 500
    column_limit: 50
    query_timezone: America/Los_Angeles
    stacking: ''
    show_value_labels: true
    label_density: 25
    legend_position: center
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: false
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    show_null_points: true
    point_style: circle
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    show_row_numbers: true
    truncate_column_names: false
    hide_totals: false
    hide_row_totals: false
    table_theme: editable
    enable_conditional_formatting: false
    conditional_formatting_include_totals: false
    conditional_formatting_include_nulls: false
    interpolation: linear
    map_plot_mode: points
    heatmap_gridlines: false
    heatmap_gridlines_empty: false
    heatmap_opacity: 0.5
    show_region_field: true
    draw_map_labels_above_data: true
    map_tile_provider: positron
    map_position: fit_data
    map_scale_indicator: 'off'
    map_pannable: true
    map_zoomable: true
    map_marker_type: circle
    map_marker_icon_name: default
    map_marker_radius_mode: proportional_value
    map_marker_units: meters
    map_marker_proportional_scale_type: linear
    map_marker_color_mode: fixed
    show_legend: true
    quantize_map_value_colors: false
    reverse_map_value_colors: false
    value_labels: legend
    label_type: labPer
    series_types: {}
    colors:
    - 'palette: Mixed Dark'
    series_colors: {}
    y_axes:
    - label: ''
      maxValue:
      minValue:
      orientation: left
      showLabels: true
      showValues: true
      tickDensity: default
      tickDensityCustom: 5
      type: linear
      unpinAxis: false
      valueFormat:
      series:
      - id: Ouch
        name: Ouch
        axisId: products.count
      - id: No Drinks this Week
        name: No Drinks this Week
        axisId: products.count
      - id: I can haz all the things
        name: I can haz all the things
        axisId: products.count
    show_dropoff: true
    hidden_fields: []
    listen:
      Date: inventory_items.created_date
      Category: products.category
    row: 8
    col: 0
    width: 8
    height: 7
  - title: aksdgha;lksdjglaksdjg;lkadjsg;lkajsglkajsdl;gkjas;lkdjgalkjlaksdjglkjasdg
    name: aksdgha;lksdjglaksdjg;lkadjsg;lkajsglkajsdl;gkjas;lkdjgalkjlaksdjglkjasdg
    model: bitbucketz
    explore: users
    type: single_value
    fields:
    - users.count
    filters:
      users.state: California
    limit: 500
    custom_color_enabled: false
    custom_color: forestgreen
    show_single_value_title: true
    show_comparison: false
    comparison_type: value
    comparison_reverse_colors: false
    show_comparison_label: true
    stacking: ''
    show_value_labels: false
    label_density: 25
    legend_position: center
    x_axis_gridlines: false
    y_axis_gridlines: true
    show_view_names: true
    limit_displayed_rows: false
    y_axis_combined: true
    show_y_axis_labels: true
    show_y_axis_ticks: true
    y_axis_tick_density: default
    y_axis_tick_density_custom: 5
    show_x_axis_label: true
    show_x_axis_ticks: true
    x_axis_scale: auto
    y_axis_scale_mode: linear
    ordering: none
    show_null_labels: false
    show_totals_labels: false
    show_silhouette: false
    totals_color: "#808080"
    series_types: {}
    single_value_title: This is a long title with lots
    hidden_fields: []
    y_axes: []
    listen:
      Date: allusers.created_date
    note_state: collapsed
    note_display: above
    note_text: asd;kgjasd;kgjaksdgnasd;kglka;sdjglkajsdglajsglk;jasdlgjasl;dgjalk;sdjgl;akjdglajdglajsdgljasdl;jglasjglkjasdlkgjalksdjglkjasg
    row: 0
    col: 0
    width: 24
    height: 2
  - name: test
    type: text
    title_text: test
    body_text: this is a test | of using pipes
    row: 15
    col: 0
    width: 8
    height: 6
  filters:
  - name: Date
    title: Date
    type: field_filter
    default_value: 7 days
    allow_multiple_values: true
    required: false
    model: bitbucketz
    explore: orders
    listens_to_filters: []
    field: orders.created_date
  - name: Category
    title: Category
    type: field_filter
    default_value: ''
    allow_multiple_values: true
    required: false
    model: bitbucketz
    explore: inventory_items
    listens_to_filters: []
    field: products.category
