
view: ndtusers{
  derived_table: {
    explore_source: users {
      column: millions {}
      column: count {}
    }
  }
  dimension: millions {
    value_format: "0.000,,M"
    type: number
  }
  dimension: count {
    type: number
  }
}

explore: ndtusers {}
